# webpack 学习 Demo

## webpack 参数

## Loader

> 模块 loader 可以链式调用，链中的每个 loader 都对资源进行转化。链会逆序执行，第一个 loader 将其转化的资源传递给下一个 loader 。


## 缓存
> 通过缓存技术，client(通常值浏览器)可以降低网络流量，使网站加载速度越快。 
+ 输出文件的文件名 （output filename）
 通过替换 output.filename 中 substitutions 设置文件名称。其中，[contenthash] substitution 将根据资源内容创建出唯一 hash。当资源内容发生变化时，[contenthash] 也会发生变化。

