
// 动态导入 （dynamic import）
async function component() {
	const element = document.createElement("div");
	const { default: _ } = await import("lodash");

	element.innerHTML = _.join(["Hello", "webpack"], "");

	element.classList.add("hello");

	return element;
}

document.body.appendChild(component());
